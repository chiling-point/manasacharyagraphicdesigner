(function() {
  "use strict";
  //user input text box
  let userInput = document.querySelector("#userInput");

  userInput.addEventListener("keyup", filterContacts);

  function filterContacts(event) {
    //Get the user input
    let userInputText = userInput.value;
    //convert it to lowercase
    userInputText = userInputText.toLowerCase();

    //get the ul and all the li children inside it
    let contactList = document.querySelector("#contactList");
    let contactListChildren = contactList.querySelectorAll(
      "li.collection-item"
    );

    //loop through all the list items and search for the name
    contactListChildren.forEach(function(currentValue, index, array) {
      //get the contact name text of each contact we are cycling through
      let currentContact = contactListChildren[index].innerText;
      //convert contact name text lowercase
      currentContact = currentContact.toLowerCase();

      //if the contact is found
      if (currentContact.search(userInputText) === 0) {
        console.log("found in " + currentContact);
        contactListChildren[index].style.display = "block";
      } else {
        contactListChildren[index].style.display = "none";
      }
    });
  }
})();

